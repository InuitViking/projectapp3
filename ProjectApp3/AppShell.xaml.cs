using ProjectApp3.Views;
using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ProjectApp3{
	public partial class AppShell : Shell{
		private readonly Random _rand = new Random();

		public AppShell(){
			InitializeComponent();
			BindingContext = this;

			// Routing.RegisterRoute("dogdetails", typeof(DogDetailPage));
			// Routing.RegisterRoute("elephantdetails", typeof(ElephantDetailPage));
		}
		
		public ICommand RandomCommand => new Command(
			async () => await DisplayAlert("Random", $"Dit tal er {_rand.Next(0, 9)}", "Ok"));
		public ICommand HelpCommand => new Command<string>(async (url) => await Launcher.OpenAsync(url));
	}
}