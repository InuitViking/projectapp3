﻿using ProjectApp3.ViewModels;
using Xamarin.Forms;

namespace ProjectApp3.Views{ // TODO: Look at https://github.com/EgonRasmussen/ShellGym/tree/3.NavigationAppShell
	public partial class MainPage{
		public MainPage(){
			InitializeComponent();
			BindingContext = new MainPageViewModel();
		}
	}
}